package shivam.menu_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    ToggleButton tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tb = (ToggleButton) findViewById(R.id.cm);
        tb.setOnCreateContextMenuListener(MainActivity.this);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case Menu.FIRST:
                Toast.makeText(MainActivity.this,"A",Toast.LENGTH_SHORT).show();
                return true;
            case Menu.FIRST+1:
                Toast.makeText(MainActivity.this,"B",Toast.LENGTH_SHORT).show();
                return true;
            case Menu.FIRST+2:
                Toast.makeText(MainActivity.this,"C",Toast.LENGTH_SHORT).show();
                return true;
            case Menu.FIRST+3:
                Toast.makeText(MainActivity.this,"D",Toast.LENGTH_SHORT).show();
                return true;
            case Menu.FIRST+4:
                Toast.makeText(MainActivity.this,"E",Toast.LENGTH_SHORT).show();
                return true;
            case Menu.FIRST+5:
                Toast.makeText(MainActivity.this,"F",Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, Menu.FIRST,Menu.NONE,"A");
        menu.add(0, Menu.FIRST+1,Menu.NONE,"B");
        menu.add(0, Menu.FIRST+2,Menu.NONE,"C");
        menu.add(0, Menu.FIRST+3,Menu.NONE,"D");
        menu.add(0, Menu.FIRST+4,Menu.NONE,"E");
        menu.add(0, Menu.FIRST+5,Menu.NONE,"F");
    }
}
